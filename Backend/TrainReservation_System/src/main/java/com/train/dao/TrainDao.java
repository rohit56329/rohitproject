package com.train.dao;

import java.util.Date;
//import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.train.Entity.Train;



@Repository
public interface TrainDao extends JpaRepository <Train, Integer> {

	//List<Train> findAllTrains();
	Train findTrainByNo(int no);
	
	//@Query(value="delete from train WHERE TrainNo=?1", nativeQuery = true)
	 
	void deleteTrainByNo(int no);
	
//	@Query(value="SELECT t.source from Train t WHERE t.TrainNo=?1",nativeQuery = true)
//	Train findtrainByNo(int no);
	
	
	@Query(value="SELECT t.TrainNo , t.TrainName  ,c.TrainDate from Train t inner join Calender c ON t.TrainNo=c.TrainNo where c.TrainDate=?1 ",nativeQuery = true )
	List <Object []> findTrainByDate(Date date);
	
	// TrainNo | TrainName      | Source | ArrivalTime | Destination | DepartureTime | TotalSeats | Fair
	
	@Query(value="  SELECT t.TrainNo, t.TrainName, t.Source, t.ArrivalTime,t.Destination,t.DepartureTime, t.TotalSeats,t.availableSeats, t.Fair\r\n"
			+ "      From Train  t inner join Calender c ON t.TrainNo=c.TrainNo"
			+ "       WHERE t.Source=?1 AND t.Destination=?2 AND c.TrainDate=?3 ",nativeQuery = true)
	List <Train> SearchTrain(String source, String Destination, Date date);
	
	@Query(value="SELECT COUNT(SeatNo) FROM seats WHERE TrainNo=?1 AND TrainDate=?2",nativeQuery = true)
	int getCount(int trainNO, Date date);
	
	
}
