package com.train.dao;



import java.util.Date;

import javax.transaction.Transactional;

//import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.train.Entity.Calender;

@Transactional
public interface CalenderDao extends JpaRepository<Calender, Integer>{

	public Calender findByTrainNo(int no);
	
	public void deleteCalenderByTrainNo(int no);

	
	@Modifying
	@Query(value="UPDATE calender set availableSeats=?1 where trainNo=?2 and trainDate=?3")
	public void updateTrainSeatcount(int availableSeats, int trainNO, Date trainDate);
	
}
