package com.train.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.train.Entity.Admin;

public interface AdminDao extends JpaRepository<Admin, Integer>{

	Admin findByEmail(String email);
//	//Admin findAdminByEmail(String email);

	
	@Query("SELECT a from Admin a WHERE a.email = ?1")
	Admin findAdminByEmail(String EMAIL);

	
	
}
