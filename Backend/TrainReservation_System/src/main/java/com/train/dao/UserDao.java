package com.train.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.train.Entity.User;

public interface UserDao  extends  JpaRepository <User, Integer>{

	User findById(int id);
	
	User findByEmail(String email);

	
	//User findUserById(int id);
	
//	@Query("select u.Email from User u WHERE u.FirstName=:n")
//	public User getUserByName(@Param("n") String name);
//	
//	@Query("SELECT u from User u WHERE u.email = ?1)
//	User findUserByEmail(String EMAIL);
	
	
	
}
