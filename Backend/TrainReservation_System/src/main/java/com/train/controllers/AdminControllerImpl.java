package com.train.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.train.Entity.Admin;
import com.train.Entity.Train;
import com.train.Entity.User;
import com.train.dao.AdminDao;
import com.train.dtos.AdminDto;
import com.train.dtos.UserDTO;
import com.train.services.AdminServiceImpl;
import com.train.services.TrainServiceImpl;

@RequestMapping("/admin")
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class AdminControllerImpl {

	@Autowired
	private AdminServiceImpl adminService;
	
	@Autowired
	private TrainServiceImpl trainService;
	
	
	
	@PostMapping("/addAdmin")
	public ResponseEntity<?> SaveAdmin(@RequestBody Admin admin) {
		Admin result= adminService.saveAdmin( admin);
		if(result!=null) 
			return Response.success(result);
		else
			return Response.error("admin not added");
	}
	
	@GetMapping("/all")
	public  ResponseEntity<?> AllAdmins()
	{
		List<Admin> admins=adminService.findAllAdmins();
		if(admins!=null) 
			return Response.success(admins);
		else
			return Response.error("admin not added");
		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> UpdateAdmin(@PathVariable("id") int id, @RequestBody AdminDto admin  )
	{
		Admin updatedAdmin= adminService.UpdateAdmin(id, admin);
		return  Response.success(updatedAdmin);
	}
	
	@GetMapping("/trainlist")
	public  @ResponseBody List<Train> trainList()
	{
		List<Train> trainlist=trainService.ShowAllTrains();
		
		return trainlist;
	}
	
	
}
