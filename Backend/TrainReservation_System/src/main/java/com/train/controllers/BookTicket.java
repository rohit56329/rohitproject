package com.train.controllers;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.train.Entity.Passenger;
import com.train.Entity.Seats;
import com.train.Entity.Ticket;
import com.train.Entity.Train;
import com.train.dao.CalenderDao;
import com.train.dao.PassengerDao;
import com.train.dao.SeatsDao;
import com.train.dao.TicketDao;
import com.train.dao.TrainDao;
import com.train.dtos.PassengerDTO;
import com.train.services.SeatServiceImpl;
import com.train.services.TicketService;
import com.train.services.TrainServiceImpl;
import com.train.services.UserServiceImpl;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class BookTicket {

	
	@Autowired
	private UserServiceImpl userService;
	
	@Autowired
	private TrainServiceImpl trainService;
	
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private SeatsDao seatsDao;
	
	@Autowired
	private SeatServiceImpl seatService;
	
	@Autowired
	private TrainDao traindao;
	
	@Autowired
	private CalenderDao calenderdao;
	
	
	
	
	
	@PostMapping("/bookTicket/{TrainNo}/{date}/{userid}")
	public ResponseEntity<?> BookTicket(@RequestBody Passenger psgr,@PathVariable("TrainNo") int tno,@PathVariable("date") Date date ,@PathVariable("userid") int userid)
	{
		Passenger passenger= userService.AddPassenger(psgr);
           
		         int UPid  =passenger.getId();
		         System.out.println(UPid);
		        // Date trainDate=Date.valueOf("date") ;
		         Date trainDate= date ;
		         int Uid= userid;
		         int TrainNO=tno;
		         String status="sucess";
		         
		         int totalSeats=trainService.FindTrainByNo(TrainNO).getTotalSeats();
		     	//public int counts= 218;
		         int count = traindao.getCount(TrainNO, trainDate);
		         int availableSeats;
		     	 int seatNo=count+1;
		     	 
		         List<Seats> seats=seatsDao.seatsByDateTrainNo(TrainNO, trainDate);
		
		
				
			  
			 
            	  Train train= trainService.FindTrainByNo(TrainNO);
            	  
					if(count<train.getTotalSeats())
					{
						
						seatService.AddAvailabelSeats(status, UPid, seatNo, TrainNO, trainDate);
					
						ticketService.BookAvailableTicket( TrainNO, Uid, UPid, status);
						//seatNo++;
						availableSeats=totalSeats-(count+1);
						//calenderdao.updateTrainSeatcount(availableSeats, TrainNO,trainDate);
						//count++ ;
						System.out.println("available seats booked  availableSeats= " +availableSeats);
					      return Response.success("available seats booked!!!   available seats="+availableSeats);
					}
					
					for(Seats canceSeat :seats)
					{
						if(canceSeat.getSeatStatus().equals("cancel") )  
						{
							int Pid=canceSeat.getPassengerId();
							
							
							
						    seatService.BookCancelSeat(status, UPid, Pid);
							ticketService.BookCancelTicket(Uid, UPid,status,Pid);
							availableSeats=totalSeats-(count+1);
							//calenderdao.updateTrainSeatcount(availableSeats, TrainNO,trainDate);
							
							
							System.out.println("cancel seats booked");
							
							return Response.success("cancel seats booked!!!");
						}
		              
						
				
			}
					{
						System.out.println("Seats are not available to book all seats are booked!!   " );
						   return Response.success("Seats are not available to book all seats are booked!! ");
					}
				
				
				
		

	      
		
	}
}
