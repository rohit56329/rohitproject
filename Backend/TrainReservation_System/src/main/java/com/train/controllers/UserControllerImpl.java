package com.train.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.train.Entity.Passenger;
import com.train.Entity.Seats;
import com.train.Entity.Ticket;
import com.train.Entity.Train;
import com.train.Entity.User;
import com.train.dao.SeatsDao;
import com.train.dao.TicketDao;
import com.train.dtos.UserDTO;
import com.train.services.TrainServiceImpl;
import com.train.services.UserServiceImpl;


@RequestMapping("/user")
@RestController  
@CrossOrigin(origins = "http://localhost:3000")
public class UserControllerImpl {

	@Autowired
	private UserServiceImpl userService;
	
	@Autowired
	private TrainServiceImpl trainService;
	

	
	
	
	//@RequestMapping(method=RequestMethod.GET, path="/users")  
	@GetMapping("/all")
	public  List<User> AllUsers()
	{
		List<User> users=userService.findAllUsers();
		
		return users;
	}
	
	@PostMapping("/signup")
	public ResponseEntity<?> SaveUser(@RequestBody User user) {
		User result= userService.saveUser( user);
		return Response.success(result);
	}
	
	@DeleteMapping("/deleteUser/{id}")
	public ResponseEntity<?> DeleteUser(@PathVariable("id") int id)
	{
		int result= userService.DeleteUser(id);
		return ResponseEntity.ok(result);
	}
	
	@PostMapping("/AddPassenger")
	public ResponseEntity<?> AddPassenger( @RequestBody Passenger psgr)
	{
		Passenger pas=userService.AddPassenger(psgr);
		return ResponseEntity.ok(pas);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") int id)
	{
		User user=userService.findUserById(id);
		return ResponseEntity.ok(user);
	}
	
	@GetMapping("/trainlist")
	public ResponseEntity<?> trainList()
	{
		List<Train> trainList= trainService.ShowAllTrains();
		return ResponseEntity.ok(trainList);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> UpdateUser(@PathVariable("id") int id, @RequestBody UserDTO user  )
	{
		User updatedUser= userService.UpdateUser(id, user);
		return  ResponseEntity.ok(updatedUser);
	}
	
	@PatchMapping("/changepass/{id}")
	public ResponseEntity<?> ChangePass(@PathVariable("id") int id, @RequestBody UserDTO pass  )
	{
		User ChangePass= userService.ChangePassword(id, pass);
		return ResponseEntity.ok(ChangePass);
		
	}
	
	
	
	
}
