package com.train.controllers;

import java.sql.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.train.Entity.Ticket;
import com.train.dao.SeatsDao;
import com.train.dao.TicketDao;
import com.train.dtos.CancelTicketDto;
import com.train.services.SeatServiceImpl;
import com.train.services.TicketService;


@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value="/ticket")
@RestController
public class TicketControllerImpl {

	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private SeatServiceImpl seatService;
	
	@Autowired
	private SeatsDao seatdao;
	
	
	@GetMapping("/details/{ticketNo}")
	public ResponseEntity<?> findTicketDetails(@PathVariable("ticketNo") int id )
	{
		List<Object[]> ticket=ticketService.findTicketDetails(id);
		return ResponseEntity.ok(ticket);
		
	}
	
	@GetMapping("/past/{userid}")
	public ResponseEntity<?> findPastTicketDetailsUser(@PathVariable("userid") int id )
	{
		List<Object[]> ticket=ticketService.findPastTicketDetails(id);
		return ResponseEntity.ok(ticket);
		
	}
	
	@GetMapping("/upcoming/{userid}")
	public ResponseEntity<?> findUpcomingTicketDetailsUser(@PathVariable("userid") int id )
	{
		List<Object[]> ticket=ticketService.findUpcomingTicketDetails(id);
		return ResponseEntity.ok(ticket);
		
	}
	@GetMapping("/all/{userid}")
	public ResponseEntity<?> findAllTicketDetailsUser(@PathVariable("userid") int id )
	{
		List<Object[]> ticket=ticketService.findAllTicketDetails(id);
		return ResponseEntity.ok(ticket);
		
	}
	
	
//	@PutMapping("/cancelTicket/{pno}/{status}")
//	public ResponseEntity<?> CancelTicket(@PathVariable("pno") int no, @PathVariable("status") String status)
//	{
//		Ticket ticket= ticketService.FindTicketByPassId(no);
//		if(ticket.getPassengerId()==no)
//		{
//	
//			ticketService.cancelTicket(no, status);
//			seatService.cancelSeat(no, status);
//			return ResponseEntity.ok("ststus changed");
//		}
//		else 
//			return ResponseEntity.ok("ticket not found");
//		
//	}
	

	@PutMapping("/cancelTicket/{sno}/{status}/{TrainNO}/{trainDate}/{pId}")
	public ResponseEntity<?> CancelTicket(@PathVariable("sno") int sno, @PathVariable("status") String status,@PathVariable("TrainNO") int TrainNO,@PathVariable("trainDate") Date trainDate,@PathVariable("pId") int pId)
	{
		seatService.CancelSeat2(status, sno, TrainNO, trainDate);
		ticketService.cancelTicket(pId, status);
			return ResponseEntity.ok("ststus changed");
		
		
	}
	
//	@PutMapping("/cancelTicket")
//	public ResponseEntity<?> CancelTicket(@RequestBody CancelTicketDto dto)
//	{
//		
//			
//		ticketService.cancelTicket(dto.getPassengerId(), dto.getStatus());
//		seatService.CancelSeat2(dto.getStatus(), dto.getSno(), dto.getTrainNO(), dto.getTrainDate());
//			
//		return ResponseEntity.ok("ststus changed");
//		
//		
//	}
}
