package com.train.controllers;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.train.Entity.Admin;
import com.train.Entity.User;
import com.train.dtos.CredentialsDTO;
import com.train.dtos.UserDTO;
import com.train.services.AdminServiceImpl;
import com.train.services.UserServiceImpl;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class LoginControllerImpl {

	@Autowired
	private UserServiceImpl userService;
	
	@Autowired
	private AdminServiceImpl adminService;
	
	

	@PostMapping("/user/login")
	public ResponseEntity<?> signInUser(@RequestBody CredentialsDTO cred) {
		User user = userService.findUserByEmailAndPassword(cred.getEmail(), cred.getPassword());
		if(user == null)
			return Response.error("user not found");
		return Response.success(user);
	}
	
	@PostMapping("/admin/login")
	public ResponseEntity<?> signInAdmin(@RequestBody CredentialsDTO cred) {
		Admin admin= adminService.findAdminByEmailAndPassword(cred.getEmail(), cred.getPassword());
		if(admin == null)
			return Response.error("admin not found");
		return Response.success(admin);
	}
	
	
//	@GetMapping( "/admin/login")
//	public ModelAndView login(Model model, Locale locale) {
//		System.out.println("Current Locale: " + locale);
//		CredentialsDTO cred = new CredentialsDTO("", "");
//		model.addAttribute("command", cred); // command obj or modelAttribute 
//		return new ModelAndView("login", "cred", cred);
//	}
//	
//	@PostMapping("/admin/login")
//	public String validate( @ModelAttribute("command") CredentialsDTO cred,Model model,HttpSession session) {
//		
//		Admin admin = adminService.findAdminByEmailAndPassword(cred.getEmail(), cred.getPassword());
//		if(admin == null)
//			return "failed";
//		session.setAttribute("admin", admin);
//		return "redirect:/trainlist";	
//	}
	
}
