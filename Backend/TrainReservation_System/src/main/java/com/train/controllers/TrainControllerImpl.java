package com.train.controllers;


import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.train.Entity.Train;
import com.train.dtos.CalenderDTO;
import com.train.dtos.Response;
import com.train.dtos.SearchtrainDto;
import com.train.dtos.TrainDto;
import com.train.services.TrainServiceImpl;

@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/train")
@RestController
public class TrainControllerImpl {

	@Autowired
	private TrainServiceImpl trainService;
	
	
	@GetMapping("/all")
	public ResponseEntity<?> trainList()
	{
		List<Train> trainlist=trainService.ShowAllTrains();
			return Response.success(trainlist);
	}
	
	@PostMapping("/addtrain")
    public ResponseEntity<?> AddTrain(@RequestBody Train train)
    {
		Train result= trainService.AddTrain(train);
		return ResponseEntity.ok(result);
    }
	
	@DeleteMapping("/delete/{no}")
	public ResponseEntity<?> Deletetrain(@PathVariable("no") int no)
	{
		int count=trainService.DeleteTrain(no);
		return ResponseEntity.ok(count);
	}
	
	@GetMapping("/trainByNo/{no}")
	public ResponseEntity<?> FindByNo(@PathVariable("no") int no)
	{
		Train train= trainService.FindTrainByNo(no);
		return ResponseEntity.ok(train);
	}
	
	@GetMapping("/trainByDate/{date}")
	public ResponseEntity<?> trainDate(@PathVariable("date") Date date )
	{
		List<Object[]> train=trainService.findtrainByDate(date);
		
		return ResponseEntity.ok(train);
	}
	
	
    @PatchMapping("/editTrain/{no}")
    public ResponseEntity<?> EditTrain(@PathVariable("no") int no, @RequestBody TrainDto train)
    {
    	Train updatedTrain= trainService.EditTrain(no, train);
    	return ResponseEntity.ok(updatedTrain);
    }
	
	@GetMapping("/{source}/{destination}/{date}")
	public ResponseEntity<?> searchTrain(@PathVariable("source")String source,@PathVariable("destination")String destination, @PathVariable("date") Date date ) {
		List<Train> train=trainService.searchTrain(source, destination, date);
		return ResponseEntity.ok(train);
	}

	
	
	@PostMapping("/search")
	public ResponseEntity<?> searchTrain2(@RequestBody SearchtrainDto searchTrain ) {
		
	
	String	source =searchTrain.getSource();
	String  destination=searchTrain.getDestination();
	Date date=searchTrain.getDate();
		List<Train> train=trainService.searchTrain(source, destination, date);
		return Response.success(train);
	}
}
