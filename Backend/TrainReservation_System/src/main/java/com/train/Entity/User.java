package com.train.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="user")
public class User {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column (name="UserId")
	private int id;
	@Column (name="FirstName")
	private String firstName;
	@Column (name="LastName")
	private String lastName;
	@Column (name="Email")
	private String email;
	@Column (name="Password")
	private String password;
	@Column (name="Phone")
	private String phone;
	@Column (name="Address")
	private String address;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE) 
	@Column (name="CreatedOn")
	private Date createdOn;
	
	@OneToMany( mappedBy = "user",fetch = FetchType.LAZY )
	private List<Ticket> ticket;
	
	
	public User()
	{
		
	}




	public User(int id, String firstName, String lastName, String email, String password, String phone, String address,
			Date createdOn) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.address = address;
		this.createdOn = createdOn;
		this.ticket= new ArrayList<Ticket>();
	}








//	public List<Ticket> getTicket() {
//		return ticket;
//	}
//
//
//
//
//	public void setTicket(List<Ticket> ticket) {
//		this.ticket = ticket;
//	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	








	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", password=" + password + ", phone=" + phone + ", address=" + address + ", createdOn=" + createdOn
				+ "]";
	}




	

	
		
	
}
