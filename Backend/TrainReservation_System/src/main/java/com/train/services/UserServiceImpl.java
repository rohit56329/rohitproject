package com.train.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.train.Entity.Passenger;
import com.train.Entity.User;
import com.train.dao.PassengerDao;
import com.train.dao.UserDao;
import com.train.dtos.CredentialsDTO;
import com.train.dtos.UserDTO;

@Transactional
@Service
public class UserServiceImpl {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private PassengerDao passengerDao;
	
	public User findUserByEmail(String email)
	{
		return userDao.findByEmail(email);
	}
	
	public User findUserById(int id)
	{
		User user =userDao.findById(id);
		
		return user;
	}
	
	public User saveUser(User user) {
		return userDao.save(user);
	}
	
	public Passenger AddPassenger(Passenger psgr)
	{
		return passengerDao.save(psgr);
	}
	
	public int DeleteUser(int id)
	{
		if(userDao.existsById(id))
		{
			userDao.deleteById(id);
			return 1;
		}
		else
		 return 0 ;
	}
	
	public List<User> findAllUsers() {
		return userDao.findAll();
	}

	public User findUserByEmailAndPassword(String email, String password) {
		User user = userDao.findByEmail(email);
		if(user != null && password.equals(user.getPassword()))
			return user;
		return null;
	}
	
   public User UpdateUser(int id, UserDTO update)
   {
	   User user = userDao.findById(id);
	 
		   if(update.getFirstName() != null) {
			   user.setFirstName(update.getFirstName());
		   }
		   
            if(update.getLastName()!=null) {
            	user.setLastName(update.getLastName());
		   }
		   
		   if(update.getEmail()!=null) {
			   user.setEmail(update.getEmail());
		   }
		   
           if(update.getPassword()!=null) {
        	   user.setPassword(update.getPassword());
		   }
 
            if(update.getAddress()==null) {
            	  user.setAddress(update.getAddress());
           }

		   
			if(update.getPhone()!=null) {
				 user.setPhone(update.getPhone());
			}
		   
			if(update.getCreatedOn()!=null) {
				 user.setCreatedOn(update.getCreatedOn());
			}
		   
		   
		   
		   User updatedUser= userDao.save(user);
		   return updatedUser ;
	
   }



public User ChangePassword(int id, UserDTO usr) {
	User user = userDao.findById(id);
	   if(user !=null)
	   { 
		   user.setPassword(usr.getPassword());
		   User ChangePass= userDao.save(user);
		   return ChangePass ;
	   }
	   else
	      return null;
}


	
	
	
}
