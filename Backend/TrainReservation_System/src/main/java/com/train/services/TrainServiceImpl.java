package com.train.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.train.Entity.Calender;
import com.train.Entity.Train;
import com.train.dao.CalenderDao;
import com.train.dao.TrainDao;
import com.train.dtos.CalenderDTO;
import com.train.dtos.DtoToEntityConverter;
import com.train.dtos.TrainDto;

@Transactional
@Service
public class TrainServiceImpl {

	@Autowired
	private TrainDao trainDao;
	
	@Autowired
	private CalenderDao calenderDao;
	
	@Autowired
	private DtoToEntityConverter convertor;
	
	
	
	public List<Train> ShowAllTrains() {
		return trainDao.findAll();
	}
	
	public List <Train> searchTrain(String source, String destination, Date date)
	{
		 List <Train> train=trainDao.SearchTrain(source, destination, date);
		 return train;
	}

	public Train AddTrain(Train train) {
		
		return trainDao.save(train);
	}
	
	public int DeleteTrain(int no)
	{
		if(trainDao.existsById(no)) {
			calenderDao.deleteCalenderByTrainNo(no);
			trainDao.deleteTrainByNo(no);
			
			return 1;
		}
		return 0;
	}
	
	public Train FindTrainByNo(int no)
	{
		Train train =trainDao.findTrainByNo(no);
		return train;
	}
	
//	public List <Object []> findtrainByDate(CalenderDTO date) {
//		Calender con=convertor.toCalenderEntity(date);
//		//System.out.println(con.getTrainDate().getDate());
//		 List <Object []> train= trainDao.findTrainByDate( con.getTrainDate());
//		 for(int i= 0; i<train.size(); i++) {
//			 Object[] obj = train.get(i);
//			 System.out.println(obj.toString());
//			 for(int j=0; j<obj.length; j++) {
//				 System.out.println((int) obj[0]);
//			 }
//		 }
//		System.out.println(train);
//		return train;
//	}
	
	
	
	
	public List<Object[]> findtrainByDate(Date date)
	{
		List<Object[]> train= trainDao.findTrainByDate(date);
		return train;
	}

	public Train EditTrain(int no, TrainDto traindto) {
		Train train =trainDao.findTrainByNo(no);
		if(train!=null)
		{
			train.setArrivalTime(traindto.getArrivalTime());
			train.setDepartureTime(traindto.getDepartureTime());
			train.setTotalSeats(traindto.getTotalSeats());
			train.setFair(traindto.getFair());
			
			Train updatedTrain =trainDao.save(train);
			return updatedTrain;
		}
		else
		 return null;
	}
	

	
}
