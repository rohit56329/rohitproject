package com.train.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.train.Entity.Seats;
import com.train.Entity.Ticket;
import com.train.dao.SeatsDao;
import com.train.dao.TicketDao;

@Transactional
@Service
public class TicketService {

	@Autowired
	private TicketDao ticketDao;
	
	
	
	public Ticket AddTicket(Ticket ticket)
	{
		return ticketDao.save(ticket);
	}
	
	
	
	
	public Ticket FindTicketByNo(int no)
	{
		Ticket ticket =ticketDao.findTicketByNo(no);
		return ticket;
	}
	
	public Ticket FindTicketByPassId(int no)
	{
		Ticket ticket =ticketDao.findTicketByPassengerId(no);
		return ticket;
	}
	
	public List<Object[]> findTicketDetails(int id)
	{
		List<Object[]> ticket=ticketDao.findTicketDetails(id);
		return ticket;
	}
	
	public List<Object[]> findPastTicketDetails(int id)
	{
		List<Object[]> ticket=ticketDao.findPastTicketDetails(id);
		return ticket;
	}
	public List<Object[]> findUpcomingTicketDetails(int id)
	{
		List<Object[]> ticket=ticketDao.findUpcomingTicketDetails(id);
		return ticket;
	}
	public List<Object[]> findAllTicketDetails(int id)
	{
		List<Object[]> ticket=ticketDao.findAllTicketDetails(id);
		return ticket;
	}
	
	
	
	public Ticket cancelTicket(int id, String status)
	{
		Ticket ticket=ticketDao.findTicketByPassengerId(id);
		ticket.setPaymentStatus(status);
		Ticket result=ticketDao.save(ticket);
		return result;
	}
	
	public void BookCancelTicket(int uid, int Upid ,String status, int pid)
	{
		ticketDao.UpdateCancelTicket(uid, Upid, status, pid);
		
	}
	
	public void BookAvailableTicket( int TrainNo ,int UId,int PId, String PaymentStatus)
	{
		ticketDao.BookAvailableTicket(TrainNo, UId, PId, PaymentStatus);
	}
	
	
	
}
