package com.train.services;

import java.sql.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.train.Entity.Seats;
import com.train.dao.SeatsDao;
import com.train.dtos.CancelTicketDto;

@Transactional
@Service
public class SeatServiceImpl {

	@Autowired
	private SeatsDao seatDao;
	
	
	public Seats AddSeat(Seats seat)
	{
		return seatDao.save(seat);
	}
	
	public Seats cancelSeat(int id, String status)
	{
		Seats seat=seatDao.findSeatByPassengerId(id);
		seat.setSeatStatus(status);
		Seats result=seatDao.save(seat);
		return result;
	}
	
	public void BookCancelSeat(String status, int UPid, int Pid)
	{
		seatDao.UpdateCancelSeat(status, UPid, Pid);
		
	}
	
	public void AddAvailabelSeats(String SeatStatus,int PassengerId,int SeatNo , int TrainNo,Date trainDate) {
		
		seatDao.AddAvailabeSeat(SeatStatus, PassengerId, SeatNo, TrainNo, trainDate);
	}
	
public void CancelSeat2(String status,int seatNo, int trainNo, Date date) {
	seatDao.cancelSeat2(status, seatNo, trainNo, date);
	}
	
	

	
}
