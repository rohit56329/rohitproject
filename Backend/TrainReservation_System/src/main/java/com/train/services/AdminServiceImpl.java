package com.train.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.train.Entity.Admin;
import com.train.Entity.User;
import com.train.dao.AdminDao;
import com.train.dtos.AdminDto;

@Transactional
@Service
public class AdminServiceImpl {

	@Autowired
	private AdminDao adminDao;
	
	public Admin findAdminByEmail(String email)
	{
		return adminDao.findAdminByEmail(email);
	}
	
	public List<Admin> findAllAdmins() {
		return adminDao.findAll();
	}
	
	public Admin findAdminByEmailAndPassword(String email, String password) {
		Admin admin = adminDao.findAdminByEmail(email);
		if(admin!= null && password.equals(admin.getPassword()))
			return admin;
		return null;
	}

	public Admin saveAdmin(Admin user) {
		Admin result=adminDao.save(user);
		return result;
	}

	public Admin UpdateAdmin(int id, AdminDto update) {
		Admin admin= adminDao.getById(id);
		 if(admin !=null)
		   {
			 admin.setFirstName(update.getFirstName());
			 admin.setLastName(update.getLastName());
			 admin.setEmail(update.getEmail());
			 admin.setPassword(update.getPassword());
			 admin.setAddress(update.getAddress());
			 admin.setPhone(update.getPhone());
			   
			   Admin updatedAdmin= adminDao.save(admin);
			   return updatedAdmin ;
		   }
		   else
			  return null ;
		
	}
}
