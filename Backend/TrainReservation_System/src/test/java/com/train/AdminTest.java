package com.train;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.train.Entity.Admin;
import com.train.dao.AdminDao;
import com.train.services.AdminServiceImpl;

@SpringBootTest
public class AdminTest {

	

	@Autowired
	private AdminServiceImpl adminService;
	@Autowired
	private AdminDao adminDao;
	
	@Test
	void testadminbyemail() {
	Admin admin=adminDao.findAdminByEmail("y@gmail.com");
		System.out.println("Admin="+admin);
		//assertThat(list).isNotEmpty();
	}
	
	@Test
	void testFindAll() {
		List<Admin> list = adminDao.findAll();
		list.forEach(System.out::println);
		//assertThat(list).isNotEmpty();
	}
	
	
	
	@Test
	void testauthenticateadmin() {
	Admin admin=adminService.findAdminByEmailAndPassword("y@gmail.com", "1234");
		System.out.println("Admin="+admin);
		//assertThat(list).isNotEmpty();
	}
}
